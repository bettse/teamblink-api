"use strict"
var qs = require('querystring');

let config = StackeryContext.deployment.config
const DEFAULT = 0

module.exports = function handler(request, output, done) {
  if (request.method != 'PATCH') {
    console.log("Ignoring non-PATCH")
    return {};
  }
  
  let iftttKey = request.resourceParams.iftttKey
  let message = {
    url: `https://io.adafruit.com/api/feeds/${iftttKey}/data`,
    headers: {
      "Content-Type": "application/json",
      "Content-Length": request.body.length,
      "X-AIO-Key": config.adafruitio_apikey
    },
    body: request.body
  }
  
  console.log("to adafruit", message)
  return output(DEFAULT, message)
  .then((wires) => {
    let wire = wires[0]
    console.log("from adafruit", wire)
    let response = {
      statusCode: 200,
      body: wire.body
    }

    return response
  })
  .catch((err) => console.error(err));
}