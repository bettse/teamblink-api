"use strict"
var http = require('http')
var qs = require('querystring');

let config = StackeryContext.deployment.config

module.exports = function handler(request, output) {
  if (request.method != 'PATCH') {
    console.log("Ignoring non-POST")
    return {};
  }
  
  let iftttKey = request.resourceParams.iftttKey.toLowerCase()
  let body = JSON.parse(request.body); //Assume json
  let color = body.value;
  let update = JSON.stringify({value: color})
  
  var options = {
    host: 'io.adafruit.com',
    path: `/api/feeds/${iftttKey}/data`,
    method: 'POST',
    headers: {
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(update),
      "X-AIO-Key": config.adafruitio_apikey
    }
  }
  
  return new Promise((resolve, reject) => {
    var req = http.request(options, (response) => {
      const rbody = [];
      response.on('data', (chunk) => rbody.push(chunk));
      response.on('end', () => resolve(rbody.join('')));
    });
    req.on('error', (err) => reject(err))
    
    req.write(update);
    req.end();
  })
  .then(() => {})
  .catch((err) => console.error(err));
}