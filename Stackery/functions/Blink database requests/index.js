"use strict"

module.exports = function handler(request, output, done) {
  var db_command;
  switch(request.method) {
  case 'GET':
    return output(0, {
      statement: "select",
      limit: 10
    }).then( (results) => {
      let result = results[0]
      let records = result.records
      return {body: JSON.stringify(records)}
    }).catch((err) => console.error(err));
  case 'POST':
  case 'PUT':  
    db_command = {
      statement: "put",
      record: JSON.parse(request.body)
    }
    return output(0, db_command).then(() => {}).catch((err) => console.error(err));
  case 'DELETE':
    db_command = {
      statement: "delete",
      where: JSON.parse(request.body)
    }
    return output(0, db_command).then(() => {}).catch((err) => console.error(err));
  }
}